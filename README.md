Machiavelli
=====
The Proficient Choice
---------------------

Machiavelli is the chess engine/gui I'm currently developing because I need stuff to do and it's pretty fun. Credit to sashavol for coding the ChessDisplayer class (mostly, I modified it a little bit to make it work on my system).

Source code: <https://bitbucket.org/LegoBricker/machiavelli/src/>

Report bugs here: <https://bitbucket.org/LegoBricker/machiavelli/issues/>

Changelog can be found in the source folders. Ill have a link when I get around to making an actual change log.

Copyright/License Notices
-------------------------

Please don't take my code and use it without giving me credit. That would make me very, very sad.

Unless otherwise noted, this software and its source code is
Copyright 2012 LegoBricker. They are dual-licensed under the 
[GNU General Public License, Version 3][1] and 
the [Educational Community License, Version 2.0][2].

It is possible that some copyright attributions may be incorrect.  
If you identify any such cases, please notify LegoBricker immediately so 
that he may correct it.

***

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

***

Licensed under the Educational Community License, Version 2.0 
(the "License"); you may not use this file except in compliance with 
the License. You may obtain a copy of the License at
	
<http://www.osedu.org/licenses/ECL-2.0>

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an "AS IS"
BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied. See the License for the specific language governing
permissions and limitations under the License.

[1]: http://www.gnu.org/licenses/gpl-3.0.html
[2]: http://www.opensource.org/licenses/ecl2.php
