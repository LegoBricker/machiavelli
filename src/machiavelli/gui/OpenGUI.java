/*
Copyright 2012 LegoBricker

This file was written by LegoBricker.
Last Modified: 12-23-12

Dual-licensed under the Educational Community License, Version 2.0 and
the GNU General Public License, Version 3 (the "Licenses"); you may
not use this file except in compliance with the Licenses. You may
obtain a copy of the Licenses at

http://www.opensource.org/licenses/ecl2.php
http://www.gnu.org/licenses/gpl-3.0.html

Unless required by applicable law or agreed to in writing,
software distributed under the Licenses are distributed on an "AS IS"
BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied. See the Licenses for the specific language governing
permissions and limitations under the Licenses.
*/

package machiavelli.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import machiavelli.engine.Board;

@SuppressWarnings("serial")
public class OpenGUI extends JPanel implements ActionListener
{

	JTextField field;
	JLabel text;
	JFrame window;
	Board board;
	
	String[] input;
	int counter;
	
	public OpenGUI(Board board2)
	{
		input = new String[4];
		counter = 0;
		board = board2;
		this.setSize(450, 100);
		this.setLayout(null);
		
		field = new JTextField();
		field.addActionListener(this);
		field.setSize(350, 50);
		field.setLocation(50, 50);
		this.add(field);
		
		text  = new JLabel("Name?");
		text.setSize(100, 25);
		text.setLocation(175, 25);
		this.add(text);
		
		window = new JFrame();
		window.setBounds(0, 0, 450, 150);
		window.setDefaultCloseOperation(3);
		window.setVisible(true);
		window.add(this);
		
		
		
	}

	/**
	 *  Handles the changing of the JLabel and puts the input into the string array.
	 */
	public void actionPerformed(ActionEvent event) 
	{
		input[counter] = field.getText();
		counter++;
		if(counter == 1)
		{
			text.setText("Date?");
		}
		else if(counter == 2)
		{
			text.setText("White or Black?");
		}
		else if(counter == 3)
		{
			text.setText("Parameters?");
		}
		else if(counter == 4)
		{
			try
			{
				new ChessDisplayer(board.getBoard(), input);
			}
			catch(FileNotFoundException e)
			{
				e.printStackTrace();
			}
				window.setVisible(false);
		}
		field.setText("");
	}

}
