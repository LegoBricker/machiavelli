/*
    Copyright 2012 sashavol/LegoBricker

    This file was written by sashavol and modified by LegoBricker.
    Last Modified: 3-11-13

    Dual-licensed under the Educational Community License, Version 2.0 and
    the GNU General Public License, Version 3 (the "Licenses"); you may
    not use this file except in compliance with the Licenses. You may
    obtain a copy of the Licenses at

    http://www.opensource.org/licenses/ecl2.php
    http://www.gnu.org/licenses/gpl-3.0.html

    Unless required by applicable law or agreed to in writing,
    software distributed under the Licenses are distributed on an "AS IS"
    BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
    or implied. See the Licenses for the specific language governing
    permissions and limitations under the Licenses.
*/

package machiavelli.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.io.FileNotFoundException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


@SuppressWarnings("serial")
public class ChessDisplayer extends JPanel 
{
	@SuppressWarnings("unused")
	private static final int EMPTY = 0;
	private static final int KING = 4;
	private static final int QUEEN = 5;
	private static final int ROOK = 1;
	private static final int BISHOP = 3;
	private static final int KNIGHT = 2;
	private static final int PAWN = 6;
	
	public static final int WIDTH = 800;
	public static final int HEIGHT= 800;
	
	public static final int WINDOWWIDTH = 1300;
	public static final int WINDOWHEIGHT = 900;
	
	public static ChessPGN pgn;
	
	private int[] grid;
	
	// Rooks
	public final java.net.URL rookw = this.getClass().getResource("rookw.png");
	public final java.net.URL rookb = this.getClass().getResource("rookb.png");
	
	// Knights
	public final java.net.URL knightw = this.getClass().getResource("knightw.png");
	public final java.net.URL knightb = this.getClass().getResource("knightb.png");
	
	// Bishops
	public final java.net.URL bishopw = this.getClass().getResource("bishopw.png");
	public final java.net.URL bishopb = this.getClass().getResource("bishopb.png");
		
	// Kings
	public final java.net.URL kingw = this.getClass().getResource("kingw.png");
	public final java.net.URL kingb = this.getClass().getResource("kingb.png");
	
	// Queens
	public final java.net.URL queenw = this.getClass().getResource("queenw.png");
	public final java.net.URL queenb = this.getClass().getResource("queenb.png");
		
	// Pawns
	public final java.net.URL pawnw = this.getClass().getResource("pawnw.png");
	public final java.net.URL pawnb = this.getClass().getResource("pawnb.png");
	
	private final Image[] chessPiecesWhite = {null,new ImageIcon(rookw).getImage(),new ImageIcon(knightw).getImage(),new ImageIcon(bishopw).getImage(),new ImageIcon(kingw).getImage(),new ImageIcon(queenw).getImage(),new ImageIcon(pawnw).getImage()};
	private final Image[] chessPiecesBlack = {null,new ImageIcon(rookb).getImage(),new ImageIcon(knightb).getImage(),new ImageIcon(bishopb).getImage(),new ImageIcon(kingb).getImage(),new ImageIcon(queenb).getImage(),new ImageIcon(pawnb).getImage()};
	
	public ChessDisplayer(int[] grid, String[] parameters) throws FileNotFoundException {
		// Do all the gui shit here! wooo lol
		
		this.grid = grid;
		final JFrame display = new JFrame("Machiavelli Chess Engine v1.0 Beta");
		display.setBounds(0, 0, WINDOWWIDTH, WINDOWHEIGHT);
		display.setDefaultCloseOperation(3);
		display.setVisible(true);
		
		JPanel container = new JPanel();
		container.setLayout(null);
		
		// Chess Board
		this.setSize(WIDTH, HEIGHT);
		this.setLocation(0, 0);
		container.add(this);
		
		// Input Field
		JTextField text = new ChessText();
		text.addActionListener((ActionListener) text);
		text.setSize(200, 30);
		text.setLocation(WIDTH + 80, 755);
		container.add(text);
		
		// PGN viewer
		pgn = new ChessPGN(parameters[0], parameters[1], parameters[2]);
		pgn.setSize(400, 400);
		pgn.setLocation(WIDTH + 80, 10);
		container.add(pgn);
		
		
		display.add(container);
		
		new Thread("Paint") {
			public void run() {
				while(true) {
					display.repaint();
					try {
						Thread.sleep(16);
					} catch (InterruptedException e) {} 
				}
			}
		}.start();
	}
	
	private int convertToGrid(int x, int y) {
		return y*8 + x;
	}
	
	@Override
	public void paint(Graphics g2) {
		super.paint(g2);
		Graphics2D g = (Graphics2D)g2;
		boolean currentShade = true;
		
		double ratioX = 8/(double)WIDTH;
		double ratioY = 8/(double)HEIGHT;
		
		for(int x = 0; x < WIDTH; x += WIDTH>>3) {
			for(int y = 0; y < HEIGHT; y+= HEIGHT>>3) {
				if(currentShade)
					g.setColor(new Color(0xEE,0x9A,0x4D));
				else
					g.setColor(new Color(0x9E,0x27,0   ));
				
				g.fillRect(x, y, WIDTH>>3, HEIGHT>>3);
				int val = grid[convertToGrid((int)Math.round(x*ratioX), (int)Math.round(y*ratioY))];

				Image draw = null;
				switch(val) {
					//white
					case KING:
						draw = chessPiecesWhite[KING];
						break;
					case QUEEN:
						draw = chessPiecesWhite[QUEEN];
						break;
					case ROOK:
						draw = chessPiecesWhite[ROOK];
						break;
					case KNIGHT:
						draw = chessPiecesWhite[KNIGHT];
						break;
					case BISHOP:
						draw = chessPiecesWhite[BISHOP];
						break;
					case PAWN:
						draw = chessPiecesWhite[PAWN];
						break;
						
					//black
					case -KING:
						draw = chessPiecesBlack[KING];
						break;
					case -QUEEN:
						draw = chessPiecesBlack[QUEEN];
						break;
					case -ROOK:
						draw = chessPiecesBlack[ROOK];
						break;
					case -KNIGHT:
						draw = chessPiecesBlack[KNIGHT];
						break;
					case -BISHOP:
						draw = chessPiecesBlack[BISHOP];
						break;
					case -PAWN:
						draw = chessPiecesBlack[PAWN];
						break;
				}
				
				if(draw != null) {
					AffineTransform temp = g.getTransform();
					g.scale((WIDTH>>3)/(double)draw.getWidth(null), (HEIGHT>>3)/(double)draw.getHeight(null));
					g.drawImage(draw,(int) Math.round(x/((WIDTH>>3)/(double)draw.getWidth(null))), (int)Math.round(y/((HEIGHT>>3)/(double)draw.getHeight(null))), null);
					g.setTransform(temp);
				}
				
				currentShade = !currentShade;
			}
			currentShade = !currentShade;
		}
	}
	
	public static ChessPGN getPGNViewer()
	{
		return pgn;
	}

}
