/*
    Copyright 2012 LegoBricker

    This file was written by LegoBricker.
    Last Modified: 12-27 -12

    Dual-licensed under the Educational Community License, Version 2.0 and
    the GNU General Public License, Version 3 (the "Licenses"); you may
    not use this file except in compliance with the Licenses. You may
    obtain a copy of the Licenses at

    http://www.opensource.org/licenses/ecl2.php
    http://www.gnu.org/licenses/gpl-3.0.html

    Unless required by applicable law or agreed to in writing,
    software distributed under the Licenses are distributed on an "AS IS"
    BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
    or implied. See the Licenses for the specific language governing
    permissions and limitations under the Licenses.
*/

package machiavelli.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;

import machiavelli.engine.Game;

@SuppressWarnings("serial")
public class ChessText extends JTextField implements ActionListener
{
	public String input;
	
	/**
	 * Changes the instance field to the input so that it can be used multiple times. 
	 *  Then sets the static text field in Game.java to the input, resets the input field, and does a text update.
	 */
	public void actionPerformed(ActionEvent event)
	{
		input = this.getText();
		Game.text = input;
		Game.onTextUpdate();
		this.setText("");
		
	}
	
}
