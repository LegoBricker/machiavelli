/*
    Copyright 2012 LegoBricker

    This file was written by LegoBricker.
    Last Modified: 12-27-12

    Dual-licensed under the Educational Community License, Version 2.0 and
    the GNU General Public License, Version 3 (the "Licenses"); you may
    not use this file except in compliance with the Licenses. You may
    obtain a copy of the Licenses at

    http://www.opensource.org/licenses/ecl2.php
    http://www.gnu.org/licenses/gpl-3.0.html

    Unless required by applicable law or agreed to in writing,
    software distributed under the Licenses are distributed on an "AS IS"
    BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
    or implied. See the Licenses for the specific language governing
    permissions and limitations under the Licenses.
*/

package machiavelli.gui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JTextArea;

import machiavelli.engine.Game;

@SuppressWarnings("serial")
public class ChessPGN extends JTextArea
{
	File file;
	String name;
	String date;
	String color;
	public ChessPGN(String newName, String newDate, String newColor)
	{
		name = newName;
		date = newDate;
		color = newColor;
		if(color.equalsIgnoreCase("white"))
		{
			Game.isWhite = true;
		}
		else
		{
			Game.isWhite = false;
		}
		file  = new File(name + "vsMachiavelli" + date.replace("/", "-") + ".pgn");
		FileReader reader;
		try
		{
			int gameCounter = 1;
			// If the file already exists, this while loop will execute which will try to make new files in numerical order
			// upwards from 1 until it doesn't exist already.
			while(file.exists())
			{
				file = new File(name + "vsMachiavelli" + date.replace("/", "-") + "-" + gameCounter + ".pgn");
				gameCounter++;
			}
			
			if(!file.exists())
			{
				file.createNewFile();
			}
			reader = new FileReader(file);
			this.read(reader, name + ".pgn");
			this.setEditable(false);
			writePGNHeader();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Gets the file that an instance of this class is using right now.
	 * @return A reference to the file used by an instance of this class.
	 */
	public File getFile()
	{
		return file;
	}
	
	public void writePGNHeader()
	{
		try 
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write("[Date \"" + date.replace("/", ".") + "\"]\n");
			if(color.equalsIgnoreCase("white"))
			{
				writer.write("[White \"" + name + "\"]\n");
				writer.write("[Black \"Machiavelli Chess Engine V1.0 Beta\"]\n");
			}
			else	
			{
				writer.write("[White \"Machiavelli Chess Engine V1.0 Beta\"]\n");
				writer.write("[Black \"" + name + "\"]\n");
			}
			writer.close();
			this.read(new FileReader(file), name + ".pgn");
			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
