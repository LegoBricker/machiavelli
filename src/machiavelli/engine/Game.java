package machiavelli.engine;
/*
Copyright 2012 LegoBricker

This file was written by LegoBricker.
Last Modified: 12-27-12

Dual-licensed under the Educational Community License, Version 2.0 and
the GNU General Public License, Version 3 (the "Licenses"); you may
not use this file except in compliance with the Licenses. You may
obtain a copy of the Licenses at

http://www.opensource.org/licenses/ecl2.php
http://www.gnu.org/licenses/gpl-3.0.html

Unless required by applicable law or agreed to in writing,
software distributed under the Licenses are distributed on an "AS IS"
BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied. See the Licenses for the specific language governing
permissions and limitations under the Licenses.
*/


import machiavelli.gui.*;

public class Game 
{
	public static Board board;
	public static String text;
	public static boolean isWhite;
	
	public static void main(String[] args)
	{
		board = new Board();
		new OpenGUI(board);
	}
	
	/**
	 *  Method that does something with the input text when the text is inputted.
	 *  If text is inputted, make sure to change the static text field to the text you want to use first.  
	 */
	public static void onTextUpdate()
	{
		int[] coords = board.interpretText(text, isWhite);
		board.makeMove(coords, isWhite);
	}

}
