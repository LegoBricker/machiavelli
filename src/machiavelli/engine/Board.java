/*
    Copyright 2012 LegoBricker

    This file was written by LegoBricker.
    Last Modified: 8-19-13

    Dual-licensed under the Educational Community License, Version 2.0 and
    the GNU General Public License, Version 3 (the "Licenses"); you may
    not use this file except in compliance with the Licenses. You may
    obtain a copy of the Licenses at

    http://www.opensource.org/licenses/ecl2.php
    http://www.gnu.org/licenses/gpl-3.0.html

    Unless required by applicable law or agreed to in writing,
    software distributed under the Licenses are distributed on an "AS IS"
    BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
    or implied. See the Licenses for the specific language governing
    permissions and limitations under the Licenses.
*/

package machiavelli.engine;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import machiavelli.gui.ChessDisplayer;
import machiavelli.gui.ChessPGN;

public class Board 
{
	
	/**
	 * The board at the start of a chess match. The numbers represent the following:
	 * -1 - black rook, -2 - black knight, -3 - black bishop, -4 - black king, -5 - black queen, -6 - black pawn
	 * 1 - white rook, 2 - white knight, 3 - white bishop, 4 - white king, 5 - white queen, 6 - white pawn
	 * 0 - empty
	 */
	public int[] startBoard = {
			-1, -2, -3, -5, -4, -3, -2, -1,
			-6, -6, -6, -6, -6, -6, -6, -6, 
			0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0,
			6, 6, 6, 6, 6, 6, 6, 6,
			1, 0, 0, 0, 4, 0, 0, 1			
	};
	
	/**
	 * The algebraic indexes of every square on the board.
	 */
	public String[] algebraicBoard = {
			"a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8",
			"a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7", 
			"a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
			"a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
			"a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
			"a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3", 
			"a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
			"a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"		
	};
	public int[] currentBoard = startBoard;
	public int[] previousBoard = new int[64];
	
	// Booleans changed to false if either rook or the king on the respective color moves.
	public boolean whiteCanCastle = true;
	public boolean blackCanCastle = true;
	
	public static int moveCounter = 0;
	
	/**
	 * Returns the board as an array.
	 * @return A pointer to the current board array.
	 */
	public int[] getBoard()
	{
		return currentBoard;
	}
	
	/**
	 *  Makes the move.
	 * @param start The starting position of a piece in the board array.
	 * @param end The ending position of the piece in the board array.
	 * @param isWhite Boolean representing if the player is white. Used to check the legality of the move before making it.
	 * @return Boolean representing whether the move succeeded or not.
	 */
	public boolean makeMove(int start, int end, boolean isWhite)
	{
		// Updates checkStatus for this player.
		isInCheck(isWhite);
		boolean succeeded = false;
		if(start == -1 || end == -1)
		{
			succeeded = false;
		}
		
		// Due to long algebraic notation, moves must be checked to be legal before being made.
		else if(isLegalMove(start, end, isWhite))
		{
			// Makes sure the start and end are within the board boundaries and then makes the end the start and the start empty.
			if((start < currentBoard.length) && (start >= 0) && (end >= 0) && (end < currentBoard.length))
			{
				// Handles castling
				if(Math.abs(start - end) == 2 && (currentBoard[start] == 4 || currentBoard[start] == -4))
				{
					// This does the same as below (so moves the king over two)
					copyBoard(previousBoard);
					int temp = currentBoard[start];
					currentBoard[start] = 0;
					currentBoard[end] = temp;
					
					// Rooks
					if(isWhite)
					{
						if(start - end == 2)
						{
							currentBoard[end - 1] = 1;
							currentBoard[end + 1] = 0;
						}
						else if(end - start == 2)
						{
							currentBoard[end + 1] = 1;
							currentBoard[end - 2] = 0;
						}
						// Change castling boolean. Once per match buddy.
						whiteCanCastle = false;
					}
					// Separate because black rook  = -1, white  = 1
					else
					{
						if(start - end == 2)
						{
							currentBoard[end - 1] = -1;
							currentBoard[end + 1] = 0;
						}
						else if(end - start == 2)
						{
							currentBoard[end + 1] = -1;
							currentBoard[end - 2] = 0;
						}
						blackCanCastle = false;
						
					}
					
					succeeded = true;
				}
				else
				{
					copyBoard(previousBoard);
					int temp = currentBoard[start];
					currentBoard[start] = 0;
					currentBoard[end] = temp;
					succeeded = true;
					
					if(temp == 4)
					{
						whiteCanCastle = false;						
					}
					else if(temp == -4)
					{
						blackCanCastle = false;
					}
				}
			}
			recordMoveToPGN(start, end, isWhite);
		}
		else
		{
			succeeded = false;
		}
		return succeeded;
		
	}
	
	/**
	 * Makes the move.
	 * @param coords An array of 2 indices with the start and end coords of a piece.
	 * @param isWhite Boolean representing if the player is white. Used to check the legality of the move before making it.
	 * @return Boolean representing whether the move succeeded or not. 
	 */
	public boolean makeMove(int[] coords, boolean isWhite)
	{
		return makeMove(coords[0], coords[1], isWhite);
	}
	
	/**
	 * A method to make a move on a completely different board other than the one stored in this instance.
	 * Only moves the pieces. Does not check if it's legal. This method is to only be used in isLegalMove
	 * for moving on a test board.
	 * @param start Start coordinate of the move.
	 * @param end End coordinate of the move.
	 * @param isWhite Boolean representing if the move is made as white or not.
	 * @param board The board as an integer array.
	 * @return Boolean representing if the move succeeded.
	 */
	public boolean makeMove(int start, int end, boolean isWhite, int[] board)
	{
		boolean succeeded = false;
		if(start == -1 || end == -1)
		{
			succeeded = false;
		}
		
		// Makes sure the start and end are within the board boundaries and then makes the end the start and the start empty.
		if((start < board.length) && (start >= 0) && (end >= 0) && (end < board.length))
		{
			int temp = board[start];
			board[start] = 0;
			board[end] = temp;
			succeeded = true;
		}
		else
		{
			succeeded = false;
		}
		return succeeded;
		
	}
	
	
	/**
	 * Interprets the text inputted and makes it viable coordinates for the make move.
	 * @param text The inputted text.
	 * @param isWhite Boolean representing whether or not white is making the move (Used when interpreting castling)
	 * @return An array of ints representing start and end coords for the makeMove.
	 */
	public int[] interpretText(String text, boolean isWhite)
	{
		int[] coords = new int[2];
		String start = "";
		String end = "";
		// Creates substrings for the starting square and ending square
		// Kingside Castling
		if(text.equals("0-0") || text.equals("O-O"))
		{
			for(int i = 0; i < currentBoard.length; i++)
			{
				// You're probably wondering why these are separate even though they do the same thing.
				// The booleans determine whether or not to stop at the black or white king.
				// Otherwise we'll always have the black king lol.
				if(currentBoard[i] == 4 && isWhite)
				{
					coords[0] = i;
					coords[1] = i + 2;
					break;
				}
				else if(currentBoard[i] == -4 && !isWhite)
				{
					coords[0] = i;
					coords[1] = i + 2;
					break;
				}
			}
		}
		// Queenside Castling
		else if(text.equals("0-0-0") || text.equals("O-O-O"))
		{
			for(int i = 0; i < currentBoard.length; i++)
			{
				if(currentBoard[i] == 4 && isWhite)
				{
					coords[0] = i;
					coords[1] = i - 2;
					break;
				}
				else if(currentBoard[i] == -4 && !isWhite)
				{
					coords[0] = i;
					coords[1] = i - 2;
					break;
				}
			}
			
		}
		else if(text.length() < 5)
		{
			coords[0] = -1;
			coords[1] = -1;
		}
		else if(text.length() == 5)
		{
			start = text.substring(0,2);
			end = text.substring(3);
		}
		else
		{
			start = text.substring(1, 3);
			end = text.substring(4);
		}
		// Finds the index representing the starting and ending square.
		for(int i = 0; i < algebraicBoard.length; i++)
		{
			if(algebraicBoard[i].equals(start))
			{
				coords[0] = i;
			}
			else if(algebraicBoard[i].equals(end))
			{
				coords[1] = i;
			}
			
		}	
		System.out.println(coords[0] + " to " + coords[1]); // Debug
		return coords;
		
		
	}
	
	/**
	 * A method that checks the legality of any given move.
	 * @param start The start coordinate of the move.
	 * @param end The end coordinate of the move.
	 * @param isWhite Boolean representing whether the move is made as white or not.
	 * @return Boolean representing if the move is legal or not.
	 */
	public boolean isLegalMove(int start, int end, boolean isWhite)
	{
		int[] futureBoard = new int[64];
		copyBoard(futureBoard);
		makeMove(start, end, isWhite, futureBoard);
		if(isInCheck(isWhite, futureBoard))
		{
			return false;
		}
		
		int piece = currentBoard[start];
		
		// Could be a switch block but whatever
		if(piece == 6 || piece == -6)
			return isLegalPawnMove(start, end, isWhite);
		else if(piece == 5 || piece == -5)
			return isLegalQueenMove(start, end, isWhite);
		else if (piece == 4 || piece == -4)
			return isLegalKingMove(start, end, isWhite);
		else if(piece == 3 || piece == -3)
			return isLegalBishopMove(start, end, isWhite);
		else if(piece == 2 || piece == -2)
			return isLegalKnightMove(start, end, isWhite);
		else if(piece == 1 || piece == -1)
			return isLegalRookMove(start, end, isWhite);
		else
			return false;
	}
	
	/**
	 * A method that checks the legality of the move.
	 * @param coords An array that contains the start and end coordinates of a move.
	 * @param isWhite Boolean representing if the move is made as a white player.
	 * @return Boolean representing if the move is legal or not.
	 */
	public boolean isLegalMove(int[] coords, boolean isWhite)
	{
		return isLegalMove(coords[0], coords[1], isWhite);
	}
	
	
	/**
	 * Determines if the pawn move is legal. Created when I split isLegalMove into smaller methods based on pieces. 
	 * @param start The starting coordinate.
	 * @param end The ending coordinate.
	 * @param isWhite Boolean representing if the move is made by white or not. 
	 * @return Boolean representing if the move is legal or not.
	 */
	public boolean isLegalPawnMove(int start, int end, boolean isWhite)
	{
		// Local variables.
		boolean moveTwo;
		boolean moveOne;
		boolean diagonalLeft;
		boolean diagonalRight;
		// Used for enpassent
		boolean pawnMovedTwoLeft;
		boolean pawnMovedTwoRight;
		
		if(isWhite)
		{
			moveTwo = (start >= 48) && (start <= 55) && (end + 16 == start); 
			moveOne = (end + 8 == start); 
			diagonalLeft = (end + 7 == start);
			diagonalRight = (end + 9 == start);
			// If the pawn to the right or left moved two the previous move, this boolean is true.
			pawnMovedTwoLeft = (currentBoard[start - 1] == -6) && (previousBoard[start - 17] == -6); 
			pawnMovedTwoRight =(currentBoard[start + 1] == -6) && (previousBoard[start - 15] == -6);
			// If you're moving one, the end has to be empty.
			if(moveOne && currentBoard[end] == 0)
				return true;
			//If you move two the end has to be empty, and the space infront can't be blocked.
			else if(moveTwo && currentBoard[end] == 0 && !(currentBoard[end+8] < 0))
				return true;
			// Otherwise you move diagonally and take a piece.
			else if((diagonalLeft || diagonalRight) && currentBoard[end] < 0)
				return true;
			else if(diagonalLeft && pawnMovedTwoLeft)
				return true;
			else if(diagonalRight && pawnMovedTwoRight)
				return true;
		}
		else
		{
			// Code identical to above.
			moveTwo = (start >= 8) && (start <= 15) && (end - 16 == start);	
			moveOne = (end - 8 == start);
			diagonalLeft = (end + 7 == start);
			diagonalRight = (end + 9 == start);
			pawnMovedTwoLeft = (currentBoard[start - 1] == 6) && (previousBoard[start + 15] == 6); 
			pawnMovedTwoRight =(currentBoard[start + 1] == 6) && (previousBoard[start + 17] == 6);
			if(moveOne && currentBoard[end] == 0)
				return true;
			//If you move two the end has to be empty, and the space infront can't be blocked.
			else if(moveTwo && currentBoard[end] == 0 && !(currentBoard[end-8] > 0))
				return true;
			else if((diagonalLeft || diagonalRight) && currentBoard[end] > 0)
				return true;
			else if(diagonalLeft && pawnMovedTwoLeft)
				return true;
			else if(diagonalRight && pawnMovedTwoRight)
				return true;
		}
		return false;
	}
	
	/**
	 * Determines if the king move is legal. Created when I split isLegalMove into smaller methods based on pieces. 
	 * @param start The starting coordinate.
	 * @param end The ending coordinate.
	 * @param isWhite Boolean representing if the move is made by white or not. 
	 * @return Boolean representing if the move is legal or not.
	 */
	public boolean isLegalKingMove(int start, int end, boolean isWhite)
	{
		// Kingside Castling
		if(end - start == 2)
		{
			if(currentBoard[start + 1] == 0 && currentBoard[start + 2] == 0)
			{
				return true;
			}
				
		}
		
		// Queenside Castling
		else if(start - end == 2)
		{	
			// Minus 3 because the rook has to move more on this side.
			if(currentBoard[start - 1] == 0 && currentBoard[start - 2] == 0 && currentBoard[start - 3] == 0)
			{
				return true;
			}
		}
		
		// Not castling
		if(isWhite)
		{
			// Literally just the code for a square around the king.
			if((currentBoard[end] <= 0) && (end == start + 1) || (end == start - 1) || (end == start + 8) || (end == start - 8) || (end == start + 9) || (end == start - 9) || (end == start - 7) || (end == start + 7))
			{
				return true;
			}
			
		}
		else
		{
			if((currentBoard[end] >= 0) && (end == start + 1) || (end == start - 1) || (end == start + 8) || (end == start - 8) || (end == start + 9) || (end == start - 9) || (end == start - 7) || (end == start + 7))
			{
				return true;
			}
			
		}
		return false;
		
	}
	
	/**
	 * Determines if the rook move is legal. Created when I split isLegalMove into smaller methods based on pieces. 
	 * @param start The starting coordinate.
	 * @param end The ending coordinate.
	 * @param isWhite Boolean representing if the move is made by white or not. 
	 * @return Boolean representing if the move is legal or not.
	 */
	public boolean isLegalRookMove(int start, int end, boolean isWhite)
	{
		boolean isInLine;
		
		// Checks horizontal alignment using integer division.
		// (Rows will give the same evaluation to integer division by 8)
		if((start / 8) == (end / 8))
			isInLine = true;
				
		// Checks vertical alignment with modulus.
		// (columns have the same remainder to division by 8)
		else if((start % 8) == (end % 8))
			isInLine = true;
		else
			isInLine = false;
		// If block checks to make sure no pieces block the rook move.
		if(isInLine)
		{
			// If they are in the same row go here.
			if(end / 8 == start / 8)
			{
				// Always loop from right to left, because it involves adding, which is easier for me to read, at least.
				if(end < start)
				{
					for(int i = end + 1; i < start; i++)
					{
						// If it reaches a piece, it makes the isInLine false, because it's not a straight line anymore.
						if(currentBoard[i] != 0)
						{
							isInLine = false;
							break; // Probably saves a few cycles here and there.
						}
					}
				}
				else if(end > start)
				{
					for(int i = start + 1; i < end; i++)
					{
						if(currentBoard[i] != 0)
						{
							isInLine = false;
							break;
						}
					}
				}
			}
			// Now for column checking.
			else if(end % 8 == start % 8)
			{
				// Once again, up to down. Involves adding not subtracting
				if(end < start)
				{
					for(int i = end + 8; i < start; i += 8)
					{
						if(currentBoard[i] != 0)
						{
							isInLine = false;
							break;
						}
					}
				}
				else if(end > start)
				{
					for(int i = start + 8; i < end; i+=8)
					{
						if(currentBoard[i] != 0)
						{
							isInLine = false;
							break;
						}
					}
					
				}				
			}
		}
		
		if(isWhite)
		{
			if(isInLine && (currentBoard[end] <= 0))
				return true;
		}
		else
		{
			if(isInLine && (currentBoard[end] >= 0))
				return true;
		}
		return false;
	}
	
	/**
	 * Determines if the bishop move is legal. Created when I split isLegalMove into smaller methods based on pieces. 
	 * @param start The starting coordinate.
	 * @param end The ending coordinate.
	 * @param isWhite Boolean representing if the move is made by white or not. 
	 * @return Boolean representing if the move is legal or not.
	 */
	public boolean isLegalBishopMove(int start, int end, boolean isWhite)
	{
		boolean isDiagonal;
		
		// Checks diagonality by checking the modulus of the start subtracted form the end. 
		// (end-start) will be divisible by 9 if its diagonal left to right, and 7 if its diagonal right to left.
		// Same as checking if the modulus of end and start to 9 or 7 are equal.
		if(((end - start) % 7 == 0) || ((end - start) % 9 == 0))
			isDiagonal = true;
		else
			isDiagonal = false;
		// If block checks to make sure no pieces block the bishop move.
		if(isDiagonal)
		{
			// If they are in the same diagonal top left to bottom right.
			if((end - start) % 9 == 0)
			{
				// Always loop from left to right, because it involves adding, which is easier for me to read, at least.
				if(end < start)
				{
					for(int i = end + 9; i < start; i += 9)
					{
						// If it reaches a piece, it makes the isDiagonal false, because it's not a straight line anymore.
						if(currentBoard[i] != 0)
						{
							isDiagonal = false;
							break; // Probably saves a few cycles here and there.
						}
					}
				}
				else if(end > start)
				{
					for(int i = start + 9; i < end; i += 9)
					{
						if(currentBoard[i] != 0)
						{
							isDiagonal = false;
							break;
						}
					}
				}
			}
			// Now for diagonals bottom left to top right.
			else if((end - start) % 7 == 0)
			{
				// Once again, up to down. Involves adding not subtracting
				if(end < start)
				{
					for(int i = end + 7; i < start; i += 7)
					{
						if(currentBoard[i] != 0)
						{
							isDiagonal = false;
							break;
						}
					}
				}
				else if(end > start)
				{
					for(int i = start + 7; i < end; i+=7)
					{
						if(currentBoard[i] != 0)
						{
							isDiagonal = false;
							break;
						}
					}				
				}				
			}
		}
		
		
		if(isWhite)
		{
			if(isDiagonal && (currentBoard[end] <= 0))
				return true;
			
		}
		else
		{
			if(isDiagonal && (currentBoard[end] >= 0))
				return true;
		}
		return false;
	}
	
	/**
	 * Determines if the queen move is legal. Created when I split isLegalMove into smaller methods based on pieces. 
	 * @param start The starting coordinate.
	 * @param end The ending coordinate.
	 * @param isWhite Boolean representing if the move is made by white or not. 
	 * @return Boolean representing if the move is legal or not.
	 */
	public boolean isLegalQueenMove(int start, int end, boolean isWhite)
	{
		if(isLegalRookMove(start, end, isWhite) || isLegalBishopMove(start, end, isWhite))
			return true;
		return false;
	}
	
	/**
	 * Determines if the knight move is legal. Created when I split isLegalMove into smaller methods based on pieces. 
	 * @param start The starting coordinate.
	 * @param end The ending coordinate.
	 * @param isWhite Boolean representing if the move is made by white or not. 
	 * @return Boolean representing if the move is legal or not.
	 */
	public boolean isLegalKnightMove(int start, int end, boolean isWhite)
	{
		int column = start % 8;
		// Moves for the left-most row in the board. (A)
		boolean leftRowMove = (start == end - 15) || (start == end - 6) || (start == end + 10) || (start == end + 17);
		// Moves for row B
		boolean almostLeftRowMove = leftRowMove || (start == end - 17) || (start == end + 15);
		// Moves for rows C-F
		boolean middleRowMove = almostLeftRowMove || (start == end - 10) || (start == end  + 6);
		// Moves for row H
		boolean rightRowMove = (start == end - 17) || (start == end - 10) || (start == end + 6) || (start == end + 15);
		// Moves for row G
		boolean almostRightRowMove = rightRowMove || (start == end - 15) || (start == end + 17);
		
		if(isWhite)
		{
			if((column == 0) && (leftRowMove))
				return true;
			else if((column == 1) && (almostLeftRowMove) && currentBoard[end] <= 0)
				return true;
			else if(((column >= 2) && (column <= 5)) && middleRowMove && currentBoard[end] <= 0)
				return true;
			else if((column == 6) && almostRightRowMove && currentBoard[end] <= 0)
				return true;
			else if((column == 7) && rightRowMove && currentBoard[end] <= 0)
				return true;
			else
				return false;
		}
		else
		{
			if((column == 0) && (leftRowMove))
				return true;
			else if((column == 1) && (almostLeftRowMove) && currentBoard[end] >= 0)
				return true;
			else if(((column >= 2) && (column <= 5)) && middleRowMove && currentBoard[end] >= 0)
				return true;
			else if((column == 6) && (almostRightRowMove) && currentBoard[end] >= 0)
				return true;
			else if((column == 7) && (rightRowMove) && currentBoard[end] >= 0)
				return true;
			else
				return false;
		}
	}
	
	/**
	 * Copies the current board into a new array. Useful if you want to test things without modifying the real board.
	 * @param newBoard The new array for the board to be put into.
	 */
	public void copyBoard(int[] newBoard)
	{
		if(newBoard.length == currentBoard.length)
		{
			for(int i = 0; i < currentBoard.length; i++)
			{
				newBoard[i] = currentBoard[i];
			}
		}
	}
	
	/**
	 * Records a move to the pgn file.
	 * @param start The start coordinate of the move.
	 * @param end The ending coordinate of the move.
	 * @param isWhite Boolean representing if the move was made by white or black.
	 */
	public void recordMoveToPGN(int start, int end, boolean isWhite)
	{
		if(isWhite)
		{
			moveCounter++;
		}
		ChessPGN tempPGN = ChessDisplayer.getPGNViewer();
		File tempFile = tempPGN.getFile();
		try 
		{
			FileWriter writer = new FileWriter(tempFile, true);
			String toWrite = "";
			if(isWhite)
			{
				toWrite += moveCounter + ". ";
			}
			// Rook
			if(previousBoard[start] == 1 || previousBoard[start] == -1)
			{
				toWrite += "R";
			}
			// Knight
			else if(previousBoard[start] == 2 || previousBoard[start] == -2)
			{
				toWrite += "N";
			}
			 // Bishop
			else if(previousBoard[start] == 3 || previousBoard[start] == -3)
			{
				toWrite += "B";
			}
			// King
			else if(previousBoard[start] == 4 || previousBoard[start] == -4)
			{
				toWrite += "K";
			}
			// Queen
			else if(previousBoard[start] == 5 || previousBoard[start] == -5)
			{
				toWrite += "Q";
			}
			
			// Indicates that a piece is taking
			// Pawns are a little different because they require the letter of the file first. 
			if((previousBoard[start] == 6 || previousBoard[start] == -6) && (previousBoard[end] != 0))
			{
				toWrite += algebraicBoard[start].substring(0, 1) + "x";
				System.out.println(toWrite);
			}
			else if(previousBoard[end] != 0)
			{
				toWrite += "x";
			}
			
			toWrite += algebraicBoard[end] + " ";
			
			/*
			 * if(somewaytocheckincheck)
			 * {
			 * 	toWrite += "+";
			 * }
			 * else if(incheckmate)
			 * {
			 * 	toWrite += "#";
			 * }
			 */
			
			writer.write(toWrite);
			writer.close();
			tempPGN.read(new FileReader(tempFile), tempFile.toString());			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * Checks to see if a player is in check or not.
	 * @param isWhite Boolean representing whether it should check white or black.
	 * @return Boolean representing if the player is in Check or not.
	 */
	public boolean isInCheck(boolean isWhite)
	{
		return isInCheck(isWhite, currentBoard);
	}
	
	/**
	 * Checks to see if a player is in check or not on any given board.
	 * @param isWhite Boolean representing whether it should check white or black.
	 * @param board The board the method should use to check.
	 * @return Boolean representing if the player is in Check or not.
	 */
	public boolean isInCheck(boolean isWhite, int[] board)
	{
		int whiteKingIndex = -1;
		int blackKingIndex = -1;
		
		// Finds the index representing the king
		for(int i = 0; i < board.length; i++)
		{
			int tempPiece = board[i];
			if(tempPiece == 4)
			{
				whiteKingIndex = i;
			}
			else if(tempPiece == -4)
			{
				blackKingIndex = i;
			}
		}
		
		// Checks if white is in check
		if(isWhite)
		{
			// Checks to see if there are pawns that are able to take the king.
			int pawnIndexOne = whiteKingIndex - 7;
			int pawnIndexTwo = whiteKingIndex - 9;
			if(pawnIndexOne >= 0 && (board[pawnIndexOne] == -6) && ((pawnIndexOne / 8) != (whiteKingIndex / 8)))
			{
				return true;
			}
			else if(pawnIndexTwo >= 0 &&(board[pawnIndexTwo] == -6) && ((pawnIndexTwo / 8) != (whiteKingIndex / 8)))
			{
				return true;
			}
			// Checks to see if you're in check by a king.
			if((whiteKingIndex == blackKingIndex + 1) || (whiteKingIndex == blackKingIndex - 1) || (whiteKingIndex == blackKingIndex + 8) || (whiteKingIndex == blackKingIndex - 8) || (whiteKingIndex == blackKingIndex + 9) || (whiteKingIndex == blackKingIndex - 9) || (whiteKingIndex == blackKingIndex - 7) || (whiteKingIndex == blackKingIndex + 7))
			{
				return true;
			}
			
			// Bishop Checking & Diagonal Queen
			// Diagonally downwards to the right.
			for(int i = whiteKingIndex + 9; i < 64; i += 9)
			{
				// If it reaches a bishop or a queen diagonally this way, then you are in check.
				if(board[i] == -3 || board[i] == -5)
				{
					return true;
				}
				// If it reaches a piece, it's not a straight line anymore.
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Up to the left.
			for(int i = whiteKingIndex - 9; i > 0; i -= 9)
			{
				// If it reaches a bishop or a queen diagonally this way, then you are in check.
				if(board[i] == -3 || board[i] == -5)
				{
					return true;
				}
				// If it reaches a piece, it's not a straight line anymore.
				else if(board[i] != 0)
				{
					break;
				}
					
			}
			// Up to the right
			for(int i = whiteKingIndex - 7; i > 0; i -= 7)
			{
				// If it reaches a bishop or a queen diagonally this way, then you are in check.
				if(board[i] == -3 || board[i] == -5)
				{
					return true;
				}
				// If it reaches a piece, it's not a straight line anymore.
				else if(board[i] != 0)
				{
					break;
				}
					
			}
			// Down to the left
			for(int i = whiteKingIndex + 7; i < 64; i += 7)
			{
				// If it reaches a bishop or a queen diagonally this way, then you are in check.
				if(board[i] == -3 || board[i] == -5)
				{
					return true;
				}
				// If it reaches a piece, it's not a straight line anymore.
				else if(board[i] != 0)
				{
					break;
				}					
			}
			// Rook and Straight Queen 
			for(int i = whiteKingIndex + 1; (i / 8) == (whiteKingIndex / 8); i++)
			{
				if(board[i] == -1 || board[i] == -5)
				{
					return true;
				}
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Rook to left
			for(int i = whiteKingIndex - 1; (i / 8) == (whiteKingIndex / 8); i--)
			{
				if(board[i] == -1 || board[i] == -5)
				{
					return true;
				}
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Rook downwards
			for(int i = whiteKingIndex + 8; i < 64; i += 8)
			{
				if(board[i] == -1 || board[i] == -5)
				{
					return true;
				}
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Rook upwards
			for(int i = whiteKingIndex - 8; i > 0; i -= 8)
			{
				if(board[i] == -1 || board[i] == -5)
				{
					return true;
				}
				else if(board[i] != 0)
				{
					break;
				}
			}

			
		}
		// Does the same for black if its black we are checking.
		else
		{
			// Checks to see if there are pawns that are able to take the king.
			int pawnIndexOne = blackKingIndex + 7;
			int pawnIndexTwo = blackKingIndex + 9;
			if((board[pawnIndexOne] == 6) && ((pawnIndexOne / 8) != (blackKingIndex / 8)))
			{
				return true;
			}
			else if((board[pawnIndexTwo] == 6) && ((pawnIndexTwo / 8) != (blackKingIndex / 8)))
			{
				return true;
			}
			// Checks to see if you're in check by a king.
			if((blackKingIndex == whiteKingIndex + 1) || (blackKingIndex == whiteKingIndex - 1) || (blackKingIndex == whiteKingIndex + 8) || (blackKingIndex == whiteKingIndex - 8) || (blackKingIndex == whiteKingIndex + 9) || (blackKingIndex == whiteKingIndex - 9) || (blackKingIndex == whiteKingIndex - 7) || (blackKingIndex == whiteKingIndex + 7))
			{
				return true;
			}
			// Bishop Checking & Diagonal Queen
			// Diagonally downwards to the right.
			for(int i = blackKingIndex + 9; i < 64; i += 9)
			{
				// If it reaches a bishop or a queen diagonally this way, then you are in check.
				if(board[i] == 3 || board[i] == 5)
				{
					return true;
				}
				// If it reaches a piece, it's not a straight line anymore.
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Up to the left.
			for(int i = blackKingIndex - 9; i > 0; i -= 9)
			{
				// If it reaches a bishop or a queen diagonally this way, then you are in check.
				if(board[i] == 3 || board[i] == 5)
				{
					return true;
				}
				// If it reaches a piece, it's not a straight line anymore.
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Up to the right
			for(int i = blackKingIndex - 7; i > 0; i -= 7)
			{
				// If it reaches a bishop or a queen diagonally this way, then you are in check.
				if(board[i] == 3 || board[i] == 5)
				{
					return true;
				}
				// If it reaches a piece, it's not a straight line anymore.
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Down to the left
			for(int i = blackKingIndex + 7; i < 64; i += 7)
			{
				// If it reaches a bishop or a queen diagonally this way, then you are in check.
				if(board[i] == 3 || board[i] == 5)
				{
					return true;
				}
				// If it reaches a piece, it's not a straight line anymore.
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Rook and Straight Queen Checking
			// Rook to right
			for(int i = blackKingIndex + 1; (i / 8) == (blackKingIndex / 8); i++)
			{
				if(board[i] == 1 || board[i] == 5)
				{
					return true;
				}
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Rook to left
			for(int i = blackKingIndex - 1; (i / 8) == (blackKingIndex / 8); i--)
			{
				if(board[i] == 1 || board[i] == 5)
				{
					return true;
				}
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Rook downwards
			for(int i = blackKingIndex + 8; i < 64; i += 8)
			{
				if(board[i] == 1 || board[i] == 5)
				{
					return true;
				}
				else if(board[i] != 0)
				{
					break;
				}
			}
			// Rook upwards
			for(int i = blackKingIndex - 8; i > 0; i -= 8)
			{
				if(board[i] == 1 || board[i] == 5)
				{
					return true;
				}
				else if(board[i] != 0)
				{
					break;
				}
			}
		}
		return false;
		
	}
}
